//
//  MediaCollectionViewControllerViewModel.swift
//  Media
//
//  Created by Yangsheng Zou on 2021-08-30.
//

import Foundation
import Combine


class MediaCollectionViewModel: ObservableObject, MediaViewModel {
    
    private var request:AnyCancellable?
    
   
    
    internal init(client: MediaClientCombine & MediaClient = DefaultMediaClient(), coordinator: MediaCoordinator?, typesOfDisplay: [FeedType] = [.audio,.text,.url,.image,.video]) {
        self.client = client
        self.coordinator = coordinator
        self.typesOfDisplay = typesOfDisplay
        updateFeed()
    }
    var client: MediaClient & MediaClientCombine
    weak var delegate: MediaCollectionViewModelDelegate?
    
    weak var coordinator: MediaCoordinator?
    
    @Published private var groupItems = [FeedType:[FeedItem]]() {
        didSet {
            delegate?.didMediaUpdated()
        }
    }
    
    private let typesOfDisplay:[FeedType]
    
    private var displayOrder = [FeedType]()
    
    
    
    var groupNum:Int {
        return groupItems.count
    }
    
    func itemsNum(type: FeedType) -> Int {
        return groupItems[type]?.count ?? 0
    }
    
    func item(type: FeedType, index:Int) -> Media? {
        return groupItems[type]?[index]
    }
    
    
    func updateFeedCombine() {
        request?.cancel()
        request = self.client.getMedia().sink { completion in
            
            switch completion {
            case .failure(let error):
                print(error)
            default:
                break
            }
        } receiveValue: {[weak self] items in
            self?.processItems(items: items)
        }

    }
    
    
    
    func updateFeed() {
       
        self.client.getMedia {[weak self] result in
            switch result {
            case .success(let items):
                self?.processItems(items: items)
            
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func processItems(items: [FeedItem]) {
        let groupItems = self.groupFeedItems(items: items)
        self.displayOrder = generateDisplayOrder(typesOfdisplay: self.typesOfDisplay, groupItems: groupItems)
        self.groupItems = groupItems
            
        
    }
    
    func generateDisplayOrder(typesOfdisplay:[FeedType], groupItems: [FeedType: [FeedItem]]) -> [FeedType] {
       
        return typesOfdisplay.filter {groupItems[$0] != nil}
    }
    
    
    
    
    
    func groupFeedItems(items: [FeedItem]) -> [FeedType:[FeedItem]] {
        return Dictionary.init(grouping: items, by: {$0.type})
        
        
    }
    
    func showDetail(item:Media) {
        coordinator?.showDetail(item: item)
    }
    
    
    
    
}




protocol MediaCollectionViewModelDelegate: AnyObject {
    func didMediaUpdated()
}
