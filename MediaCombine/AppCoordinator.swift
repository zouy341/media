//
//  AppCoordinator.swift
//  MediaCombine
//
//  Created by Yangsheng Zou on 2021-09-13.
//

import Foundation
import UIKit
import SafariServices
import AVKit
import SwiftUI


class AppCoordinator: Coordinator,MediaCoordinator {
    
    
    weak var parent: Coordinator? = nil
    var navigation: UINavigationController
    var children: [Coordinator] = []
    
   
    
    func start() {
        Task {
            for i in 0...100 {
                print(i)
            }
        }
        
       print("!!!!")
        
        let vm = MediaCollectionViewModel(coordinator: self)
        let vc = UIHostingController(rootView: MediaContentView(viewModel: vm))
        navigation.pushViewController(vc, animated: true)
        
        
    }
    
   
    
    
    func showDetail(item: Media) {
        switch item.type {
        case .url:
            guard let url = URL(string: item.data) else {return}
            let vc = SFSafariViewController(url: url)
            navigation.pushViewController(vc, animated: true)
        case .video:
            guard let url = URL(string: item.data) else {return}
            let player = AVPlayer(url: url)
            let vc = AVPlayerViewController()
            vc.player = player
            navigation.present(vc, animated: true) {
                vc.player?.play()
            }
        default:
            break
        }
    }
    
    
    internal init(navigation: UINavigationController) {
        self.navigation = navigation
    }
}
