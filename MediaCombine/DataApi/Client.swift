//
//  Client.swift
//  Media
//
//  Created by Yangsheng Zou on 2021-08-30.
//

import Foundation
import Combine

protocol Client {
    var baseURL: URL { get }
    var urlSession: URLSession {get}
    func call<EndpointType: Endpoint>(for endpoint: EndpointType, completion: @escaping (Result<EndpointType.DataType, Error>) -> Void) -> URLSessionDataTask?
    func callCombine<EndpointType: Endpoint>(for endpoint: EndpointType) -> AnyPublisher<EndpointType.DataType, Error>
    
}


extension Client {
    var urlSession: URLSession {
        return URLSession.shared
    }
    
    
    func callCombine<EndpointType: Endpoint>(for endpoint: EndpointType) -> AnyPublisher<EndpointType.DataType, Error> {
        
        let request = endpoint.makeRequest(baseURL: baseURL)
        return  urlSession.dataTaskPublisher(for: request).map {$0.data}.decode(type: EndpointType.DataType.self, decoder: endpoint.decoder).receive(on: DispatchQueue.main).eraseToAnyPublisher()
        
        
    }
    
    
    func call<EndpointType: Endpoint>(for endpoint: EndpointType, completion: @escaping (Result<EndpointType.DataType, Error>) -> Void) -> URLSessionDataTask? {
        
        
        let request = endpoint.makeRequest(baseURL: baseURL)
        
        let task = urlSession.dataTask(with: request) { data, _, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let data = data else {
                let error = NSError(domain: "InvalidDataError", code: 0, userInfo: nil)
                completion(.failure(error))
                return
            }
            
            let result = Result {
                try endpoint.decoder.decode(EndpointType.DataType.self, from: data)
            }
            
            completion(result)
        }
        task.resume()
        return task
        
    }
}
