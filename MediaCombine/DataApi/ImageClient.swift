//
//  ImageClient.swift
//  Media
//
//  Created by Yangsheng Zou on 2021-09-06.
//

import Foundation
import UIKit

protocol ImageDownloader {
    func getImage(url:URL, completion: @escaping (Result<UIImage,Error>) -> Void) -> URLSessionDataTask
    var session:URLSession {get}
}

extension ImageDownloader {
    
    
}


class defaultImageDownloader:ImageDownloader  {
    
    enum ImageLoadError:Error {
        case noData
        case dataError
    }
    
    var session: URLSession {
        return URLSession.shared
    }
    
    func getImage(url: URL, completion: @escaping (Result<UIImage, Error>) -> Void) -> URLSessionDataTask {
        let task = session.dataTask(with: url, completionHandler: { imageData,_,error in
            
            if let error = error {
                completion(.failure(error))
                return
            }
            
            
            
            guard let imageData = imageData else {
                completion(.failure(ImageLoadError.noData))
                return
            }
            
            guard let image = UIImage(data: imageData) else {
                completion(.failure(ImageLoadError.dataError))
                return
            }
            
            completion(.success(image))
            
        })
        task.resume()
        return task
    }
    
    
}


