//
//  Endpoint.swift
//  Media
//
//  Created by Yangsheng Zou on 2021-08-30.
//

import Foundation
import Combine

protocol Endpoint {
    associatedtype DataType: Decodable
    var path: String { get }
    var queryItems: [URLQueryItem] { get }
    associatedtype DecoderType: TopLevelDecoder
    var decoder: DecoderType {get}
    func makeRequest(baseURL: URL) -> URLRequest
    var method: String {get}
    
    
    
}

extension Endpoint {
    var decoder:JSONDecoder {
        return JSONDecoder()
    }
   
    var method:String {return "GET"}
    func makeRequest(baseURL: URL) -> URLRequest {
        guard var components = URLComponents(url: baseURL.appendingPathComponent(path), resolvingAgainstBaseURL: false) else {
            fatalError("Unable to create URL components \(self)")
        }
                
        components.path = "/" + path
        components.queryItems = queryItems.isEmpty ? nil : queryItems
        
        guard let url = components.url else {
            fatalError("Could not get url from \(components)")
        }
        var request = URLRequest(url: url)
        request.httpMethod = self.method
        return URLRequest(url: url)
    }
}


