//
//  MediaEndpoint.swift
//  Media
//
//  Created by Yangsheng Zou on 2021-08-30.
//

import Foundation


struct MediaEndpoint:Endpoint {
    typealias DataType = [FeedItem]
    static let shared = MediaEndpoint()
    
    var path: String {
        "/data.json"
    }
    
    var queryItems: [URLQueryItem] {
        return []
    }
    
    
    var decoder: JSONDecoder {
        return JSONDecoder()
    }
}
