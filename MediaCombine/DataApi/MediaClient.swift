//
//  MediaClient.swift
//  Media
//
//  Created by Yangsheng Zou on 2021-08-30.
//

import Foundation
import Combine

protocol MediaClient {
    func getMedia(completion:@escaping (Result<[FeedItem], Error>) -> Void)
   
}

protocol MediaClientCombine {
    func getMedia() -> AnyPublisher<[FeedItem],Error>
}
    
    


class DefaultMediaClient:MediaClient,Client,MediaClientCombine {
    
    var feedItemsEndPoint: MediaEndpoint
    
   
    
    internal init(feedItemsEndPoint: MediaEndpoint = MediaEndpoint()) {
        self.feedItemsEndPoint = feedItemsEndPoint
    }
    
    func getMedia() -> AnyPublisher<[FeedItem],Error>  {
        callCombine(for: feedItemsEndPoint)
    }
    
    
    
    
    
    
    func getMedia(completion: @escaping (Result<[FeedItem], Error>) -> Void) {
        _ = self.call(for: feedItemsEndPoint) { result in
            completion(result)
        }
    }

    
    var baseURL: URL {
        return  URL(string:  "https://ptest-165de-default-rtdb.firebaseio.com")!
    }
    
    
}


class MockMediaClient:MediaClient,MediaClientCombine {
    func getMedia() -> AnyPublisher<[FeedItem], Error> {
        //return Just(mockItems).setFailureType(to: Error.self).eraseToAnyPublisher()
        Result.Publisher(.success(mockItems)).eraseToAnyPublisher()
    
        
    }
    
    internal init(mockItems: [FeedItem]) {
        self.mockItems = mockItems
    }
    
    
    
    
    func getMedia(completion: @escaping (Result<[FeedItem], Error>) -> Void) {
        let result = Result.init {
            mockItems
        }
        completion(result)
    }
    
    
    
    let mockItems: [FeedItem]
    
}

