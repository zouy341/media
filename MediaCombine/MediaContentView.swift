//
//  ContentView.swift
//  MediaCombine
//
//  Created by Yangsheng Zou on 2021-09-11.
//

import SwiftUI

struct MediaContentView<T:MediaViewModel>: View {
    
    @ObservedObject var viewModel: T
    

    var body: some View {
        List {
            
        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        MediaContentView(viewModel: MediaCollectionViewModel(coordinator: nil))
    }
}


protocol MediaViewModel: ObservableObject {
    var groupNum:Int {get}
    
    func updateFeedCombine()
    
}


