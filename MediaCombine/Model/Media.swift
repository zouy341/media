//
//  Media.swift
//  Media
//
//  Created by Yangsheng Zou on 2021-08-30.
//

import Foundation


protocol Media {
    var type:FeedType {get}
    var data:String {get}
}

enum FeedType:String, Decodable {
    case video
    case text
    case image
    case url
    case audio
}


struct FeedItem: Decodable,Media {
    let type:FeedType
    let data:String
}

