//
//  Coordinator.swift
//  MediaCombine
//
//  Created by Yangsheng Zou on 2021-09-13.
//

import Foundation




protocol Coordinator:AnyObject {
    var parent:Coordinator? {get}
    var children: [Coordinator] {get set}
    func start()
}




protocol MediaCoordinator:AnyObject {
    func showDetail(item:Media)
}
